FROM bde2020/spark-python-template:3.1.2-hadoop3.2

COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

WORKDIR /app

ADD gunicorn_starter.sh gunicorn_starter.sh
ENTRYPOINT ["./gunicorn_starter.sh"]
